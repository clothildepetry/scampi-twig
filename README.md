# Scampi-Twig

[Scampi-Twig](https://pidila.gitlab.io/scampi-twig) est un outil de création de pages statiques ou de gabarits d’intégration html/css/js utilisant la bibliothèque de composants [Scampi](https://gitlab.com/pidila/scampi) pour les CSS et Twig pour la génération des pages html. Il est développé et maintenu par le [PiDILA](https://pidila.gitlab.io), le pôle intégration html de la Direction de l'information légale et administrative.

Scampi-Twig autorise la saisie des contenus par inclusion de fragments, en saisie twig/html directe ou par inclusion de fichiers markdown.

[Documentation complète](https://pidila.gitlab.io/scampi-twig)

Prérequis
------------------------------------------------------------------------------

Nodejs, npm et gulp installé en global.


Aperçu rapide
------------------------------------------------------------------------------

```bash
$ git clone --recursive https://gitlab.com/pidila/scampi-twig.git <mon_projet>
$ cd mon_projet && npm install && gulp dev
```

[Commencer un nouveau projet](https://pidila.gitlab.io/scampi-twig/installation.html).

Créer les pages html avec Twig
------------------------------------------------------------------------------

Le fichier `dev/templates/layout/base.twig` rassemble les éléments communs à tous les projets.

Le fichier `dev/templates/layout/base-projet.twig` hérite de la base.twig et ajoute les particularités communes à toutes les pages d’un projet.

Le fichier `dev/pages/index.twig` sert de centre d’aiguillage pour les pages prêtes à être recettées.

Le répertoire *dev/templates/pages/* accueille les pages complètes. Trois pages sont livrées avec le dépôt : celle pour le styleguide, qui sera à compléter avec les éléments propres au projet, la page d'index et une page 404.

* tâche unitaire : `gulp make:html`

### Prise en charge de markdown

Les contenus en markdown peuvent être insérés de deux façons :

  * en saisie directe dans un template :
  
  ```
  {% markdown %}
    Contenu
  {% endmarkdown %}
  ```

  * par inclusion de fichier depuis un template :
  
  ```
  {% markdown "chemin/relatif/depuis/templates" %}{% endmarkdown %}
  ```


Gérer les fichiers javascript
------------------------------------------------------------------------------

Une tâche gulp prend en charge les scripts externes prérequis (jquery, modernizr) pour les déplacer dans le projet :

* tâche unitaire : `gulp copy:js-vendors`

Lorsque vous utilisez un module de scampi comportant un script, recopiez-le dans *dev/_assets/scripts/main/*. Le script du module anchor-focus, qui gère un bug de Chrome/Safari/IE, s'y trouve déjà.

Ces scripts seront concaténés et envoyés dans le build, soit par la tâche unitaire, soit par la tâche globale de build.

* tâche unitaire : `gulp make:js-main`


Compiler les css
------------------------------------------------------------------------------

Un fichier prêt à l’emploi est présent dans *dev/_assets/projet/scss*. Pour personnaliser un projet :

  1. recopier dans *projet* le fichier core-scampi-settings, le renommer et le personnaliser, l’importer en lieu et place de celui de scampi ;
  2. dans *style.css* décommenter les imports de modules scampi utilisés tels quels ;
  3. ajouter les modules et partials propres au projet.

Tâches gulp :

  * tâche unitaire : `gulp make:css`


Traiter les icones svg
------------------------------------------------------------------------------

Si les gabarits comportent des icônes au format svg, elles doivent être placées dans *dev/_assets/project/icones/unitaires/*. À titre d’exemple, 3 icônes y sont déjà placées.

Une tâche gulp permet de générer depuis ce répertoire un sprite d’icones svg à utiliser dans les gabarits :

  * `gulp prep:sprite` transforme les icones unitaires en sprite *(icon-sprite.svg)* et le place dans *dev/_assets/project/icones/*

Le sprite sera déplacé avec les autres ressources statiques quand on effectuera la commande gulp générale `gulp copy:assets`.

Voir aussi le [tutoriel pour la préparation et la fabrication du sprite d'icônes](https://pidila.gitlab.io/scampi-twig/documentation/sprite-svg.html).


Déplacer les fichiers statiques
------------------------------------------------------------------------------

Les éléments du répertoire *dev/_assets/project* (à l’exclusion des scss et des scripts) sont déplacés tels quels dans le répertoire public grâce à des tâches gulp.

  * `gulp copy:assets` pour tous les éléments du répertoire _assets/project
  * `gulp copy:favicon` place la favicon à la racine du site


Récapitulatif des tâches gulp principales
------------------------------------------------------------------------------

On accède à la liste des tâches gulp disponibles et leur résumé à l'aide de la commande `gulp`.

  * Tout construire   :   `gulp build`
  * Tout nettoyer     :   `gulp clean`
  * Compiler sass     :   `gulp make:css`
  * Compiler twig     :   `gulp make:html`
  * Concaténer les js :   `gulp make:js-main`

Pendant les développements on peut lancer une tâche qui builde puis lance un serveur, ouvre la page d’index et surveille les modifications effectuées sur les css et le html :

  * `gulp dev`

Déploiement sur <user>.gitlab.io/<projet>
------------------------------------------------------------------------------

Placer à la racine du dépôt un fichier *.gitlab-ci.yml* avec le contenu suivant :

```yaml
image: node
variables:
  GIT_SUBMODULE_STRATEGY: recursive
pages:
  stage: deploy
  script:
  - npm install
  - ./node_modules/.bin/gulp build:public
  artifacts:
    paths:
    - public
  only:
  - master
```

  * image : équipe le package docker qui sera utilisé pour "mouliner" les fichiers avant de les envoyer sur le serveur
  * pages : crée une tâche nommée *pages* qui demande au "runner" d’effectuer sur les fichiers du dépôt les tâches définies par la section *script* puis de déployer des fichiers produits dans un répertoire *public* dès qu’un commit est poussé sur *master*.

On peut surveiller ces opérations dans l’onglet CI / CD > jobs du dépôt.

Pour plus d’informations, voir [ce tuto pas à pas](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/).

**À noter :** Les pages déployées ne peuvent être protégées par un mot de passe, même si le dépôt est privé. Penser à ajouter des metas attributs noindex nofollow dans le head des pages.


Contact
------------------------------------------------------------------------------

Benoît Dequick - Hugues Moreno - Anne Cavalier. Nous écrire : prenom.nom@dila.gouv.fr ou l'adresse commune de l'équipe : pidila@dila.gouv.fr

### Liste d’échanges et d’information :

[S’abonner](https://framalistes.org/sympa/subscribe/pidila-tools).


Licence
------------------------------------------------------------------------------

Scampi-twig est distribué sous une double licence :[MIT](https://gitlab.com/pidila/scampi-twig/blob/master/LICENCE-MIT.md) et [CeCILL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html).

Vous pouvez utiliser Scampi avec l’une ou l’autre licence.
