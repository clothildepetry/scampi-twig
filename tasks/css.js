'use strict';

var gulp         = require('gulp-help')(require('gulp'));
var runSequence  = require('run-sequence');
var config       = require('../config.json');

var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps   = require('gulp-sourcemaps');
var minCss       = require('gulp-clean-css');
var rename       = require("gulp-rename");

// make:css
// Compile les css
// ----------------------------------------------------------------------------
gulp.task('make:css',"Compile les css", function(callback) {
  return gulp.src(config.paths.assets + 'project/scss/**/*.scss')
  .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest(config.paths.build + 'assets/css'))
  .pipe(minCss())
  .pipe(rename({ extname: '.min.css' }))
  .pipe(gulp.dest(config.paths.build + 'assets/css'))
  callback()
});
