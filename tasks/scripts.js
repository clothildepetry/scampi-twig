'use strict';

var gulp         = require('gulp-help')(require('gulp'));
var runSequence  = require('run-sequence');
var config       = require('../config.json');
var concat       = require('gulp-concat');

// copy:js-vendors
// Copie les dépendences javascript dans `public`
// ----------------------------------------------------------------------------
gulp.task('copy:js-vendors', "Copie les dépendences js", function() {
  gulp.src(config.paths.assets + 'scampi/js/libs/modernizr.js')
    .pipe(gulp.dest(config.paths.build + 'assets/scripts/vendors'));
  gulp.src('./node_modules/jquery/dist/jquery.min.js')
    .pipe(gulp.dest(config.paths.build + 'assets/scripts/vendors'));
});

// make:js
// Concaténation des scripts du projet (scripts/main/)
// ----------------------------------------------------------------------------
gulp.task('make:js-main',"Concatène tous les fichiers présents dans dev/projet/scripts/main", function() {
  gulp.src(config.paths.assets + 'project/scripts/main/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest(config.paths.build + 'assets/scripts'))
});
