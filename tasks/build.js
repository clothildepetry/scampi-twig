'use strict';

var gulp         = require('gulp-help')(require('gulp'));
var config       = require('../config.json');
var runSequence  = require('run-sequence');
var argv         = require('yargs').argv;

// variables liées à l'environnement
if (argv.prod) {
  var baseURL = config.baseURL.prod,
      env = "prod";
}
else {
  var baseURL = config.baseURL.dev;
}

// build
// Construit le répertoire public
// ----------------------------------------------------------------------------
gulp.task('build',"Construit le répertoire `public`", function(callback) {
  runSequence(
    'clean',
    ['make:html-prettify',
    'copy:assets','copy:favicon','copy:js-vendors',
    'make:js-main','make:css'],
    callback);
  },
  {
    options: {
      'dev': '[build par défaut] baseURL = config.baseURL.dev',
      'prod': 'baseURL = config.baseURL.dev',
    }
  }
);

// dev
// Enchaîne les tâches build + live
// ----------------------------------------------------------------------------
gulp.task('dev',"build + live", function(callback) {
  runSequence(
    'build',
    'live',
    callback)
});
