'use strict';

var gulp          = require('gulp-help')(require('gulp'));
var runSequence   = require('run-sequence');
var config        = require('../config.json');
var configPackage = require('../package.json');
var configScampi  = require('../dev/assets/scampi/package.json');
var argv          = require('yargs').argv;

var twig          = require('gulp-twig');
var twigMarkdown  = require('twig-markdown');
var prettify      = require('gulp-jsbeautifier');


// variables selon l'environnement
if (argv.prod) {
  var env = "prod",
      baseURL = config.baseURL.prod;
}
else {
  var baseURL = config.baseURL.dev;
}

// make:html
// Compile twig vers html
// ----------------------------------------------------------------------------
gulp.task('make:html',"Compile le html", function() {
  return gulp.src(config.paths.pages + '**/*.twig')
    .pipe(twig({
      base: config.paths.templates,
      data: {baseURL, env, config, configPackage, configScampi},
      extend: twigMarkdown
    }))
    .pipe(gulp.dest(config.paths.build));
  },
  {
    options: {
      'dev': '[par défaut] baseURL = config.baseURL.dev',
      'prod': 'baseURL = config.baseURL.dev',
    }
  }
);

// make:html-prettify
// Compile twig puis normalise l'indentation du html
// ----------------------------------------------------------------------------
gulp.task('make:html-prettify',"Normalise l'indentation du html",['make:html'], function() {
  gulp.src(config.paths.build + '**/*.html')
    .pipe(prettify({
      indent_size: 2,
      max_preserve_newlines: 1
    }))
    .pipe(gulp.dest(config.paths.build));
});
